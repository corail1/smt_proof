;---------- inputs ----------
(define-const Tp Int  11)
(define-const Cp Int   1)
(define-const Tr Int  17)
(define-const Cc Int   2)
(define-const t  Int 100) 
;------------------------------
(define-const Cr Int (+ Cp Cc))
(declare-const i Int)
(declare-const j Int)
(define-const x Int (+ (* i Tp) (* j Tr)))
(define-const r Int (+ (* i Cp) (* j Cr) Cr))
;-----
(assert (< x t))
(assert (< 0 i))
(assert (< 0 j))
(assert (< 0 r))
;---------- optimize ----------
(maximize r)
(check-sat)
; returns sat
; rbf(100) = 19
(get-value (r)) ; 19
(get-value (i)) ; 1
(get-value (j)) ; 5
(get-value (x)) ; 96
