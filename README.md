# SMT Proofs

This repository contains the SMT-lib2 modelization of a polling task as presented in "Real-Time Polling Task: Design and Analysis", and files used for the proofs of the formulas given in the article.
