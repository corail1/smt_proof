(define-fun max ((a Int) (b Int)) Int
    (ite (>= a b) a b)
)
(define-fun ceiling ((a Int) (b Int)) Int
(let ((k (div a b)))
    (ite (= a (* k b)) k (+ k 1))
)
)
;---------- inputs ----------
(declare-const Tp Int)
(declare-const Cp Int)
(declare-const Tr Int)
(declare-const Cc Int)
(declare-const t  Int)
(define-const Cr Int (+ Cp Cc))
(assert (> Tp 0))
(assert (> Cp 0))
(assert (> Tr 0))
(assert (> Cc 0))
;------------------------------
(declare-const i Int)
(declare-const j Int)
(define-const x Int (+ (* i Tp) (* j Tr)))
(define-const r Int (+ (* i Cp) (* j Cr) Cr))
;-----
(assert (>= x 0))
(assert (< x t))
(assert (<= 0 i))
(assert (<= 0 j))
(assert (<= 0 r))
;---------- Running domination ----------
(assert (>= Tp Tr))
(define-const rbf Int 
    (* (ceiling t Tr) Cr)
)
(assert (not (>= rbf r)))
;---------- bounded ----------
(assert (<= t 100))
;---------- solve ----------
(check-sat)
; returns sat
; rbf(100) = 19
(get-value (Tp))
(get-value (Tr))
(get-value (Cp))
(get-value (Cr))

(get-value (i))
(get-value (j))
(get-value (x))
(get-value (r))

(get-value (rbf))